<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_should_import"
 * )
 */
class LibraryShouldImport extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $future_events_ids = \Drupal::state()->get('findit_library_sync.imported_future_event_ids', []);

    if (in_array($value, $future_events_ids)) {
      $message = "Library event with ID '$value' is part of a repeating event that has already been imported. Operation skipped.";
      throw new MigrateSkipRowException($message);
    }

    return TRUE;
  }
}
