<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_get_ages"
 * )
 */
class LibraryGetAges extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $raw_audience = $row->get($this->configuration['audience']);

    return $this->mapAges($raw_audience, $migrate_executable, $row);
  }

  protected function mapAges($raw_audience, MigrateExecutableInterface $migrate_executable, Row $row) {
    if (!is_array($raw_audience)) {
      $library_event_id = $row->get('src_unique_id');
      $migrate_executable->saveMessage("Library event with ID '$library_event_id' does not specify an audience. Ages cannot be determined. Using Adult.", MigrationInterface::MESSAGE_WARNING);
      $row->setDestinationProperty('findit_library_sync_error_type', 'FINDIT_LIBRARY_SYNC_NO_AGE_IN_SOURCE');
      $raw_audience = [['name' => 'Adult']];
    }

    $audience = array_column($raw_audience, 'name');

    $ages_tids = [];

    foreach ($audience as $value) {
      switch ($value) {
        case 'Children':
          $ages_tids = array_merge($ages_tids, range(216, 229)); // Infant - 13.
          break;

        case 'Teen':
        case 'Teen (ages 12-18)':
          $ages_tids = array_merge($ages_tids, range(228, 234)); // 12 - 18.
          break;

        case 'Adult':
          $ages_tids = array_merge($ages_tids, range(234, 236)); // 18 - Senior Adult.
          $ages_tids[] = 243; // Young Adult.
          break;

        case 'All Ages':
          $ages_tids = array_merge($ages_tids, range(215, 236)); // Pre-natal - Senior Adult.
          $ages_tids[] = 243; // Young Adult.
          break;

        case 'Preschool':
          $ages_tids = array_merge($ages_tids, range(216, 220)); // Infant - 4.
          break;

        case 'Middle School':
          $ages_tids = array_merge($ages_tids, range(226, 229)); // 10 - 13.
          break;

        case 'Seniors':
          $ages_tids[] = 236; // Senior Adult.
          break;

        case 'Young Children (ages 0-5)':
          $ages_tids = array_merge($ages_tids, range(216, 221));
          break;

        case 'School Children (ages 5-11)':
          $ages_tids = array_merge($ages_tids, range(221, 227));
          break;


        default:
          \Drupal::logger('findit_library_sync')->info(
            "Library event @library_url includes the @audience audience which has not been mapped to any age values.",
            [
              '@library_url' => $row->getSourceProperty('src_website'),
              '@audience' => $value,
            ]
          );
          break;
      }
    }

    return array_unique($ages_tids);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }
}
