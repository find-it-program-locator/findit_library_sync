<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_file\Plugin\migrate\process\ImageImport;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_logo_import"
 * )
 */
class LibraryLogoImport extends ImageImport {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      $value = $this->getDefaultLogoURL();
      $image = parent::transform($value, $migrate_executable, $row, $destination_property);

      if ($image) {
        $image['alt'] = "Cambridge Public Library city skyline logo.";
      }
    } else {
      $image = parent::transform($value, $migrate_executable, $row, $destination_property);

      if ($image) {
        $image['alt'] = "Event image for " . $row->getSourceProperty('src_title');
      }
    }

    return $image;
  }

  protected function getDefaultLogoURL() {
    $url = implode('/',
      [
        \Drupal::request()->getSchemeAndHttpHost(),
        drupal_get_path('module', 'findit_library_sync'),
        'CambridgePublicLibrary.png',
      ]);
    return $url;
  }
}
