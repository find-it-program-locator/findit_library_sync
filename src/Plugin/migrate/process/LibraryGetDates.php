<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_get_dates"
 * )
 */
class LibraryGetDates extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $raw_start = $row->get($this->configuration['start']);
    $raw_end = $row->get($this->configuration['end']);
    $raw_future_events = $row->get($this->configuration['future_dates']);
    $processed_dates = [];

    if (isset($this->configuration['limit'])) {
      // 1 is subtracted from future events, because current item is included in
      // limit count.
      $raw_future_events = array_slice($raw_future_events, 0, $this->configuration['limit'] - 1);
    }

    $fromFormat = 'Y-m-d\TH:i:sP';
    $toFormat = 'U'; // Smart date is UNIX time.  Formerly used: 'Y-m-d\TH:i:s';
    $from_timezone = 'America/New_York';
    $to_timezone = 'UTC';

    $transformed_start = DateTimePlus::createFromFormat($fromFormat, $raw_start, $from_timezone); //
    $transformed_end = DateTimePlus::createFromFormat($fromFormat, $raw_end, $from_timezone); //

    $processed_dates[] = [
      'start_date' => $transformed_start->format($toFormat, ['timezone' => $to_timezone]),
      'end_date' => $transformed_end->format($toFormat, ['timezone' => $to_timezone]),
    ];

    $interval = $transformed_start->diff($transformed_end);

    foreach ($raw_future_events as $raw_future_event) {
      $future_date_start = DateTimePlus::createFromFormat($fromFormat, $raw_future_event['start'], $from_timezone);
      $future_date_end = clone $future_date_start;
      $future_date_end->add($interval);

      $processed_dates[] = [
        'future_id' => $raw_future_event['event_id'],
        'start_date' => $future_date_start->format($toFormat, ['timezone' => $to_timezone]),
        'end_date' => $future_date_end->format($toFormat, ['timezone' => $to_timezone]),
      ];
    }

    return $processed_dates;
  }

}
