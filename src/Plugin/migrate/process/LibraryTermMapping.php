<?php

namespace Drupal\findit_library_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_library_term_mapping"
 * )
 */
class LibraryTermMapping extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $term_mapping = $row->get($this->configuration['term_mapping']);
    $skip_list = $row->get($this->configuration['skip_list']);
    $location = $row->get($this->configuration['location']);
    $raw_categories = $row->get($this->configuration['categories']);
    $categories = is_array($raw_categories) ? array_column($raw_categories, 'name') : [];
    $raw_audience_list = $row->get($this->configuration['audience']);
    $audience_list = is_array($raw_audience_list) ? array_column($raw_audience_list, 'name') : [];
    $audience_list[] = '_audience_independent_';

    if (!empty($skip_list['categories']) && array_intersect($categories, $skip_list['categories'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_category_list = implode(', ', $skip_list['categories']);
      $message = "Library event with ID '$library_event_id' includes one of the excluded categories: $flat_category_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    if (!empty($skip_list['locations']) && in_array($location, $skip_list['locations'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_location_list = implode(', ', $skip_list['locations']);
      $message = "Library event with ID '$library_event_id' happens at one of the excluded locations: $flat_location_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    if (!empty($skip_list['audience']) && array_intersect($audience_list, $skip_list['audience'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_audience_list = implode(', ', $skip_list['audience']);
      $message = "Library event with ID '$library_event_id' belongs to one of the excluded audience: $flat_audience_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    $activities_tids = [];
    foreach ($term_mapping as $term_id => $mapping) {
      if (isset($mapping['locations'][$location]) && array_intersect($categories, $mapping['locations'][$location])) {
        $activities_tids[] = $term_id;
      }
      foreach ($audience_list as $audience) {
        if (isset($mapping['audience'][$audience]) && array_intersect($categories, $mapping['audience'][$audience])) {
          $activities_tids[] = $term_id;
        }
      }
    }

    return array_unique($activities_tids);
  }
}
