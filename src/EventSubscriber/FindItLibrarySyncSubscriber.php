<?php

namespace Drupal\findit_library_sync\EventSubscriber;

use Drupal\Core\Url;
use Drupal\Core\Site\Settings;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigrateRollbackEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber.
 */
class FindItLibrarySyncSubscriber implements EventSubscriberInterface {

  /**
   * Adds OAuth2 authentication data.
   */
  public function onPreImport(MigrateImportEvent $event) {
    $migration = $event->getMigration();
    $tags = $migration->getMigrationTags();

    if (in_array('FindIt Library Auth', $tags)) {
      $source_config = $migration->getSourceConfiguration();
      $source_config['authentication']['client_id'] = Settings::get('findit_library_client_id');
      $source_config['authentication']['client_secret'] =  Settings::get('findit_library_client_secret');
      $migration->set('source', $source_config);
    }
  }

  /**
   * Persists future event ids to skip.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event) {
    $row = $event->getRow();

    $ids = $event->getDestinationIdValues();
    $nid = reset($ids);

    $created_nids = \Drupal::state()->get('findit_library_sync.created_nids', []);
    $created_nids[] = $nid;
    \Drupal::state()->set('findit_library_sync.created_nids', $created_nids);

    if (!empty($row->getDestination()['findit_library_sync_error_type'])) {
      switch ($row->getDestination()['findit_library_sync_error_type']) {
        case 'FINDIT_LIBRARY_SYNC_NO_AGE_IN_SOURCE':
          \Drupal::logger('findit_library_sync')->warning(
            "Library event with ID @library_event_id does not specify an audience. Ages could not be determined. Adult was assumed. Drupal URL @node_url Library URL @library_url",
            [
              '@library_event_id' => $row->getSourceProperty('src_unique_id'),
              '@node_url' => Url::fromRoute('entity.node.canonical', ['node' => $nid], ['absolute' => TRUE])->toString(),
              '@library_url' => $row->getSourceProperty('src_website'),
            ]
          );
          break;
      }
    }

    if (!empty($row->getDestination()['pseudo_library_dates'])) {
      $future_events_ids = \Drupal::state()->get('findit_library_sync.imported_future_event_ids', []);
      $future_events_ids = array_unique(array_merge($future_events_ids, array_column($row->getDestination()['pseudo_library_dates'], 'future_id')));
      \Drupal::state()->set('findit_library_sync.imported_future_event_ids', $future_events_ids);
    }
  }

  /**
   * Deletes future event ids cache.
   */
  public function onPostRollback(MigrateRollbackEvent $event) {
    \Drupal::state()->delete('findit_library_sync.created_nids');
    \Drupal::state()->delete('findit_library_sync.imported_future_event_ids');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    $events[MigrateEvents::PRE_IMPORT][] = ['onPreImport'];
    $events[MigrateEvents::POST_ROW_SAVE] = ['onPostRowSave'];
    $events[MigrateEvents::POST_ROLLBACK][] = ['onPostRollback'];

    return $events;
  }
}
